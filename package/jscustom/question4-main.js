// Vidljivi objekti:
// userData - mapa s podatcima koji se perzistiraju na poslužitelju
// questionCompleted - booleova zastavica

// metoda koja se poziva pri inicijalizaciji pitanja; u userData se može upisati sve što je potrebno...
function questionInitialize() {
  userData.content = /*@#import#(init${iid}.txt)*/;
}

// metoda koja se poziva kako bi napravila prikaz potrebne stranice; dobiva kao argument pogled koji je zadao korisnik
function questionRenderView(vid,infoParams) {
  if(vid=="start") {
    res = {};
    res.options = ['cont'];
    res.questionState = JSON.stringify({enteredContent: userData.content});
    res.view = {action: "page:1"};
    return res;
  }
  if(vid=="done") {
    return {
      options: ['cont'],
      questionState: JSON.stringify({}),
      view: {action: "page:3"}
    };
  }
  if(vid=="notdone") {
    return {
      options: ['ret'],
      questionState: JSON.stringify({}),
      view: {action: "page:2"},
      variables: {fw: infoParams.fw, wr: infoParams.wr, wb: infoParams.wb}
    };
  }
  if(vid=="error") {
    return {
      options: ['ret'],
      questionState: JSON.stringify({}),
      view: {action: "page:4"}
    };
  }
  return null;
}

// metoda koja se poziva kako bi se obradio poslan odgovor; prima kljuc (tj. opciju koja je pritisnuta) i objekt s poslanim podatcima
function questionProcessKey(vid, key, sentData) {
  if(vid=="start") {
    var expected = /*@#import#(answer${iid}.txt)*/;
    if(!checkDimensions(sentData.enteredContent,expected.length,expected[0].length)) {
      return {action: "view:error"};
    }
    userData.content = sentData.enteredContent;
    var checkResult = isAnswerCorrect(userData.content, expected);
    if(checkResult.firstWrong==-1) {
      return {action: "view:done", completed: true};
    } else {
      return {action: "view:notdone", info: {fw: checkResult.firstWrong, wr: checkResult.wrongRows, wb: checkResult.wrongBits}};
    }
  } else if(vid=="done") {
    if(!questionCompleted) {
      return {action: "view:start"};
    } else {
      return {action: "next"};
    }
  } else if(vid=="notdone") {
    return {action: "view:start"};
  } else if(vid=="error") {
    return {action: "view:start"};
  } else {
    return {action: "fail"};
  }
}

function checkDimensions(c,rows,cols) {
  if(typeof(c) == 'undefined' || c == null) return false;
  for(var r = 0; r < rows; r++) {
    if(typeof(c[r]) == 'undefined' || c[r] == null) return false;
    if(c[r].length != cols) return false;
  }
  return true;
}

function isAnswerCorrect(answer,expected) {
        var firstWrong = -1;
        var wrongRows = 0;
        var wrongBits = 0;
        for(var r = 0; r < expected.length; r++) {
          var erow = expected[r];
          var arow = answer[r];
          var wrongrowbits = 0;
          for(var c = 0; c < erow.length; c++) {
            if(erow[c]!=arow[c]) wrongrowbits++;
          }
          if(wrongrowbits>0) {
            if(firstWrong==-1) firstWrong=r;
            wrongRows++;
            wrongBits += wrongrowbits;
          }
        }
	return {firstWrong: firstWrong, wrongRows: wrongRows, wrongBits: wrongBits};
}

