    // 'interactive': if set to false, address bit values will not be shown
    // 'protectContent': if set to true, data content will not be changeable by mouse 
    // if 'interactive' is set to false, 'protectContent' will automatically become true
    // 'inLabels': array of labels to be displayed with address inputs; ignored if null
    // 'outLabels': array of labels to be displayed with data outputs; ignored if null
    // 'extraLeftOffset': amount of extra horizontal shift to allow fine tuning of displayed image when inLabels are used
    function KoderDekoder(addressBits, dataBits, canvasName, currentAddress, currentContent, interactive, protectContent, inLabels, outLabels, extraLeftOffset) {
      this.addressBits = addressBits;
      this.dataBits = dataBits;
      this.canvas = document.getElementById(canvasName);
      this.context = this.canvas.getContext("2d");
      this.inLabels = inLabels;
      this.outLabels = outLabels;
      this.panelSize = 20;
      this.panelPanelGap = 5;
      this.panelBodyGap = 20;
      this.decoderWidth = 80;
      this.rowHeight = 24;
      this.decoderVerticalMargin = 10;
      this.extraLeftOffset = extraLeftOffset;

      this.decoderCoderGap = 50;

      this.coderBitWidth = 20;
      this.coderLeftGap = 15;
      this.coderRightGap = 15;
      this.coderRightText = 20;
      this.coderDataHanging = 10;
      this.coderDataHeight = this.rowHeight-4;

      this.interactive = interactive;
      this.protectContent = interactive ? protectContent : true;

      if(currentAddress!=null) {
        this.address = currentAddress; // position 0 holds MSB
      } else {
        this.address = []; // position 0 holds MSB
        for(var i = 0; i < this.addressBits; i++) { this.address[i] = 0; }
      }
      if(currentContent!=null) {
        this.content = currentContent; // position 0 holds MSB
      } else {
        this.data = []; // position 0 holds MSB
        for(var i = 0; i < this.dataBits; i++) { this.data[i] = 0; }
        this.content = initContent();
      }

      this.activeRegions = new ActiveRegions();
      connectCanvasActiveRegion(this, this.activeRegions);

      function initContent() {
        var rows = 1 << addressBits;
        var content = [];
        for(var r = 0; r < rows; r++) {
          content[r] = [];
          for(var c = 0; c < dataBits; c++) {
            content[r][c] = 0;
          }
        }
        return content;
      }

      this.getContent = function() {
        return this.content;
      }

      this.paint = function() {

        this.context.textAlign = "start";
        this.context.textBaseline = "alphabetic";

        var activeRow = binToInt(this.address);

        this.activeRegions.reset();

        // Set floating topleft corner...
        var xoff = 10+this.extraLeftOffset;
        var yoff = 25;

        var rows = 1 << this.addressBits;
        var decoderHeight = rows * this.rowHeight + 2 * this.decoderVerticalMargin;
        var decoder = {x: xoff+this.panelSize+this.panelBodyGap, y: yoff, w: this.decoderWidth, h: decoderHeight};

        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

        this.context.lineWidth = 1;
        this.context.strokeStyle = "black";
        this.context.strokeRect(0, 0, this.canvas.width, this.canvas.height);

        // Draw decoder body:
        this.context.strokeRect(decoder.x, decoder.y, decoder.w, decoder.h);

        // Draw decoder address panels:
        this.context.font="12px Georgia";
        this.context.textBaseline = "middle";
        for(var i = 0; i < this.addressBits; i++) {
          var h1 = this.addressBits * this.panelSize + (this.addressBits-1) * this.panelPanelGap;
          var h2 = (decoderHeight - h1)/2 + decoder.y;
          var y = i*(this.panelSize+this.panelPanelGap);

          this.context.fillStyle = "#eeeeee";
          this.context.fillRect(decoder.x-this.panelBodyGap-this.panelSize, h2+y, this.panelSize, this.panelSize);
          this.context.strokeStyle = "black";
          this.context.strokeRect(decoder.x-this.panelBodyGap-this.panelSize, h2+y, this.panelSize, this.panelSize);

          this.context.beginPath();
          this.context.moveTo(decoder.x-this.panelBodyGap, h2+y+this.panelSize/2);
          this.context.lineTo(decoder.x, h2+y+this.panelSize/2);
          this.context.stroke();

          this.context.fillStyle = "#000000";
          if(this.interactive) {
            this.activeRegions.add(new ActiveRegion(decoder.x-this.panelBodyGap-this.panelSize, h2+y, this.panelSize, this.panelSize, {a: this.address, i: i, execute: function() {this.a[this.i]=1-this.a[this.i];}}));
            this.context.textAlign = "center";
            this.context.fillText(this.address[i], decoder.x-this.panelBodyGap-this.panelSize/2, h2+y+this.panelSize/2);
          }
          this.context.textAlign = "start";
          this.context.fillText("A"+(this.addressBits-1-i), decoder.x+4, h2+y+this.panelSize/2);
          if(this.inLabels!=null) {
            this.context.textAlign = "end";
            this.context.fillText(this.inLabels[i], decoder.x-this.panelBodyGap-this.panelSize-4, h2+y+this.panelSize/2);
          }
        }

        var coder = {x: decoder.x+this.decoderWidth + this.decoderCoderGap, y: decoder.y, w: this.coderLeftGap + this.dataBits*this.coderBitWidth + this.coderRightText +this.coderRightGap, h: decoder.h};

        // Draw coder body:
        this.context.strokeRect(coder.x, coder.y, coder.w, coder.h);

        // Draw element names:
        this.context.font="16px Georgia";
        this.context.textBaseline = "bottom";
        this.context.textAlign = "center";
        this.context.fillText("DEKODER", decoder.x+decoder.w/2, decoder.y -2);
        this.context.fillText("KODER",   coder.x+coder.w/2, coder.y -2);

        // Draw decoder-coder lines:
        this.context.font="12px Georgia";
        for(var i = 0; i < rows; i++) {
          this.context.strokeStyle = this.interactive && activeRow==i ? "red" : "black";
          this.context.beginPath();
          this.context.moveTo(decoder.x+this.decoderWidth, decoder.y + this.decoderVerticalMargin + i*this.rowHeight + this.rowHeight/2);
          this.context.lineTo(decoder.x+this.decoderWidth + this.decoderCoderGap, decoder.y + this.decoderVerticalMargin + i*this.rowHeight + this.rowHeight/2);
          this.context.stroke();
          this.context.textBaseline = "middle";
          this.context.textAlign = "end";
          this.context.fillText("O"+i, decoder.x+this.decoderWidth-4, decoder.y + this.decoderVerticalMargin + i*this.rowHeight + this.rowHeight/2);
          if(this.interactive) {
            this.context.textAlign = "start";
            this.context.textBaseline = "bottom";
            this.context.fillText(i==activeRow ? "1" : "0", decoder.x+this.decoderWidth+4, decoder.y + this.decoderVerticalMargin + i*this.rowHeight + this.rowHeight/2);
          }
        }

        // Draw bit lines:
        this.context.strokeStyle = "black";
        this.context.font="12px Georgia";
        this.context.textBaseline = "top";
        this.context.textAlign = "center";
        for(var i = 0; i < this.dataBits; i++) {
          var x = coder.x + this.coderLeftGap + i*this.coderBitWidth + this.coderBitWidth/2;
          this.context.beginPath();
          this.context.moveTo(x, coder.y + this.decoderVerticalMargin + this.rowHeight/2);
          this.context.lineTo(x, coder.y + coder.h + this.coderDataHanging);
          this.context.stroke();
          this.context.fillStyle = "#000000";
          this.context.fillText("D"+(this.dataBits-1-i), x, coder.y + coder.h + this.coderDataHanging);
          if(this.interactive) {
            this.context.fillText(this.content[activeRow][i], x, coder.y + coder.h + this.coderDataHanging+15);
          }
          if(this.outLabels!=null) {
            this.context.fillText(this.outLabels[i], x, coder.y + coder.h + this.coderDataHanging+30);
          }
        }

        // Draw data cells:
        this.context.textBaseline = "middle";
        for(var r = 0; r < rows; r++) {
          var baseY = coder.y + this.decoderVerticalMargin + r*this.rowHeight + this.rowHeight/2;
          for(var c = 0; c < this.dataBits; c++) {
            this.context.fillStyle = this.interactive && r==activeRow ? "#eeaaaa" : "#eeeeee";
            this.context.fillRect(coder.x+this.coderLeftGap+c*this.coderBitWidth, baseY - this.coderDataHeight/2, this.coderBitWidth, this.coderDataHeight);
            this.context.strokeStyle = "black";
            this.context.strokeRect(coder.x+this.coderLeftGap+c*this.coderBitWidth, baseY - this.coderDataHeight/2, this.coderBitWidth, this.coderDataHeight);

            this.context.fillStyle = "#000000";
            this.context.textAlign = "center";
            this.context.fillText(this.content[r][c], coder.x+this.coderLeftGap+c*this.coderBitWidth+this.coderBitWidth/2, baseY);

            if(!this.protectContent) {
              this.activeRegions.add(new ActiveRegion(coder.x+this.coderLeftGap+c*this.coderBitWidth, baseY - this.coderDataHeight/2, this.coderBitWidth, this.coderDataHeight, {content: this.content, r: r, c: c, execute: function() {this.content[this.r][this.c]=1-this.content[this.r][this.c];}}));
            }
          }

          this.context.textAlign = "right";
          this.context.fillText(binToHex(this.content[r]), coder.x+coder.w-this.coderLeftGap, baseY);
        }
      }

      this.paint();
    }


