      // Convert array of bits to integer value; arr[0] is MSB
      function binToInt(arr) {
        var res = 0;
        for(var i = 0; i < arr.length; i++) {
          res = 2*res + arr[i];
        }
        return res;
      }

      // Convert array of arbitrary number of bits to hexdigit string; arr[0] is MSB
      function binToHex(arr) {
        if(arr.length==0) return "";
        var res = "";
        var groups = Math.trunc(arr.length/4);
        var first = arr.length - groups*4;
        if(first!=0) {
          res += binToHexDigit(arr.slice(0,first));
        }
        for(var i = 0; i < groups; i++) {
          res += binToHexDigit(arr.slice(first, first+4));
          first += 4;
        }
        return res;
      }

      // Convert array of bits which represents a single hex-digit to hexdigit (as string); arr[0] is MSB
      function binToHexDigit(arr) {
        var i = binToInt(arr);
        if(i<10) return String.fromCharCode(48+i);
        return String.fromCharCode(65+i-10);
      }

      // Convert small integer numbers into array of bits; if array is not wide enough, only adequate number of bits is returned
      // If 'msbFirst'=true, arr[0] will hold MSB, otherwise MSB will be last bit in array.
      function intToBin(n, arr, msbFirst) {
        var br = n;
        var index = 0;
        if(msbFirst) {
          while(index < arr.length) {
            arr[arr.length-1-index] = br & 1 == 1 ? 1 : 0;
            br = br >> 1;
            index++;
          }
        } else {
          while(index < arr.length) {
            arr[index] = br & 1 == 1 ? 1 : 0;
            br = br >> 1;
            index++;
          }
        }
      }

