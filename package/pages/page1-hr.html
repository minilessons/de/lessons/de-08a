<p>Kroz prethodne cjeline upoznali smo se s osnovnim logičkim sklopovima, a potom i sa 
   standardnim kombinacijskim modulima. Jednostavnije i često korištene sklopovske 
   podsustave ima smisla graditi uporabom tih komponenata.
</p>

<p>Krenemo li u razvoj složenijih sklopovskih projekata, broj komponenata koje bismo 
   trebali iskoristiti može postati neprihvatljivo velik. U tom slučaju, možemo posegnuti
   za programirljivim modulima.
</p>

<p><em>Programirljivi moduli</em> (engl. <i>Programmable Logic Devices</i>, PLD) su 
   sklopovske komponente koje sadrže veću količinu
   sklopovlja čiji je rad na određen način moguće podešavati (konfigurirati) kako bi se 
   dobila željena funkcionalnost. Ideja je pri tome pronaći određene sklopovske strukture
   koje se s jedne strane mogu jeftino proizvoditi u velikom broju a koje istovremeno 
   omogućavaju da se postupkom konfiguriranja podese tako da mogu ostvarivati proizvoljne 
   Booleove funkcije.
</p>

<p>U današnje vrijeme uobičajeno je da korisnik željenu funkcionalnost zadaje u nekom
   od formalnih jezika za opisivanje sklopovlja. Na slici 1, primjer je prikazan u jeziku
   VHDL. Sljedeći korak je pokretanje računalnog programa koji nazivamo sintetizator.
   Taj program kao ulaz dobiva informacije o programirljivom modulu te željeni opis
   funkcionalnosti te provodi postupak sinteze: na temelju danih ulaza određuje kako
   treba konfigurirati programirljivi modul da bi isti obavljao traženu funkcionalnost
   te rezultat zapisuje u datoteku koja pohranjuje niz konfiguracijskih bitova
   (takozvani engl. <i>bitstream</i>). Na kraju, uređaju koji se naziva programator
   predaje se konfiguracijska datoteka te sam programirljivi modul; uređaj u modul upisuje
   danu konfiguraciju nakon čega programirljivi modul obavlja definirane funkcije.
</p>

<figure class="mlFigure">
<img src="@#file#(progwork.svg)" alt="Konfiguriranje programirljivog modula"><br>
<figcaption><span class="mlFigKey">Slika 1.</span> Konfiguriranje programirljivog modula.</figcaption>
</figure>

<p>Ovisno o izvedbi modula, njihovo programiranje moguće je obaviti:</p>
<ul>
<li>u tvornici prilikom završne izrade samog programirljivog modula,</li>
<li>u zasebnim uređajima koje nazivamo <i>programatorima</i> ili pak</li>
<li>izravno u digitalnom podsustavu u koji su već ugrađeni.</li>
</ul>

<p>U prvom slučaju radi se o modulima koji su tehnološki tako izvedeni da se
   neovisno o funkciji koju modul obavlja, tijekom proizvodnje prolazi kroz
   niz zajedničkih koraka, a sama se funkcionalnost definira jednom od završnih 
   koraka. Uzevši u obzir da se isti proizvodni proces može koristiti za izradu
   mnoštva takvih modula pa potom za pojedinog naručitelja napraviti samo drugačiji
   završni korak proizvodnje prije pakiranja u kućište čipa, moduli se mogu proizvoditi
   u velikim serijama uz relativno povoljnu cijenu. Jednom kad ovakav modul izađe iz
   tvornice, on je konfiguriran i za korisnika obavlja skup funkcija koje je korisnik
   tražio od proizvođača.
</p>

<p>Drugi slučaj pokriva module koji tvronicu napuštaju zapakirani u konačne čipove
   ali koji još uvijek nisu programirani. Za programiranje ovakvih modula korisnik
   mora nabaviti poseban uređaj - programator - u koji će umetnuti čip, pokrenuti 
   postupak programiranja (koji se tada obavlja električkim putem), izvaditi čip 
   iz programatora i potom ga ugraditi u digitalni sustav u kojem će isti biti korišten
   (npr. zalemiti ga na tiskanu pločicu na kojoj se nalazi i ostatak digitalnog 
   sklopovlja). Primjer programatora za programirljive module određenih porodica
   EPROM-a te GAL-ova prikazan je na slici 2. Zeleni centralni dio predstavlja utor
   u koji je potrebno umetnuti nožice čipa programirljive komponente.
</p>

<figure class="mlFigure">
<img src="@#file#(univprog.jpg)" alt="Programator jednostavnijih programirljivih modula"><br>
<figcaption><span class="mlFigKey">Slika 2.</span> Programator jednostavnijih programirljivih modula.</figcaption>
</figure>

<p>Posljednji navedeni slučaj odnosi se na napredniji podskup programirljivih modula
   koji tvornicu napuštaju kao neprogramirani i koje korisnik odmah i ugrađuje u
   digitalni sustav u kojem će biti korišteni te potom na odgovarajući način 
   programiranje (konfiguriranje) pokreće izravno u ciljnom sustavu (nakon ugradnje).
   Primjer je prikazan na slici 3 gdje je prikazana FER ULXP pločica (slikana s obje 
   strane). Programirljiva komponenta (čip FPGA) vidljiva je na gornjem dijelu slike
   (veliki crni čip jako puno nožica smješten s desne strane) gdje je već ugrađena u7
   samu pločicu (zalemljena). Za potrebe programiranja tog čipa, isti se ne odlemljuje
   i ne vadi s pločice. 
</p>

<figure class="mlFigure">
<img src="@#file#(ferulxp.jpg)" alt="FER ULXP pločica s FPGA čipom"><br>
<figcaption><span class="mlFigKey">Slika 3.</span> FER ULXP pločica s FPGA čipom.</figcaption>
</figure>


<p>Od navedenih slučajeva, slika 1 pokriva drugi i treći slučaj. U drugom slučaju,
   korisnik bi na računalu specificirao željenu funkcionalnost, pokrenuo sintetizator
   i dobio konfiguracijsku datoteku, potom na neki način na računalo priključio i 
   programator (primjerice, putem USB-priključka), u programator fizički umetnuo
   programirljivi modul, na računalu pokrenuo program koji zna komunicirati s 
   programatorom, predao mu konfiguracijsku datoteku i pokrenuo postupak programiranja.
</p>
<p>U trećem slučaju, koraci su slični samo što nema posebnog uređaja za programiranje:
   na računalo se povezuje čitav digitalni sustav u koji je ugrađena i programirljiva
   komponenta (opet primjerice putem sučelja USB) te se nakon toga pokreće postupak
   programiranja.
</p>

<div class="mlNote">
 <p>Studenti koji na FER-u u okviru kolegija Digitalna logika rade sklopovsku inačicu 
    laboratorijskih vježbi rade ovaj posljednji 
    slučaj. Nakon što definiraju funkcionalnost sklopa crtanjem sheme ili pisanjem VHDL
    koda, pokreću u alatu <i>Lattice Diamond</i> postupak sinteze koji kao konačni 
    rezultat na disk zapisuje konfiguracijsku datoteku (ekstenzije JED, jer je zapisana
    u skladu s JEDEC-specifikacijom). Potom na računalo putem USB sučelja priključuju 
    FPGA-pločicu koja sadrži programirljiv modul (čip FPGA) te čitav niz drugog 
    sklopovlja. Konačno, na računalu pokreću program <span class="command">ujprog</span>
    kojem predaju putanju do konfiguracijske datoteke i koji izravno na FPGA-pločici
    tim podatcima konfigurira FPGA-čip.
  </p>
</div>

<p>O tehnološkim implementacijskim detaljima vezanim uz način izrade i konfiguriranja
   programirljivih modula detaljnije ćemo pričati na predavanjima.
</p>




   


